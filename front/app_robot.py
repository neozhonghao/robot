import os
import requests
from secrets import token_hex

class App_Robot(object):
    def __init__(self):
        # insert robot api url
        self.url = ""

    def create_dir(self):
        """
        Creates a temporary storage if not available
        PARAMS: None
        RETURNS: 
            path: str, path of the storage
        """
        if not os.path.isdir('upload_data'):
            os.mkdir('upload_data')
        if not os.path.isdir('upload_data/robot'):
            os.mkdir('upload_data/robot')
        token = token_hex(nbytes=16)
        path = os.path.join('upload_data/robot',token)
        return path

    def post(self,path):
        """
        Post the request to the robot api
        PARAMS: path: str, path to the commands file
        RETURNS: 
            results: str, output from the robot app
        """
        f_in = open(path, "rb")
        files = {"file.txt":("file.txt", f_in)}
        resp = requests.post(self.url,files=files)
        f_in.close()
        results = resp.json()["out"]
        return results

    def post_file(self,file):
        """
        Post the request using a text file
        PARAMS: file: werkzeug.datastructures.FileStorage object
        RETURNS: 
            results: str, output from the robot app
        """        
        path = self.create_dir()
        file.save(path)
        results = self.post(path)
        if os.path.isfile(path):
            os.remove(path)
        return results

    def post_cmd(self,cmd):
        """
        Post the request using a string command
        PARAMS: cmd: str
        RETURNS: 
            results: str, output from the robot app
        """        
        path = self.create_dir()
        f = open(path, 'w')
        f.write(cmd)
        f.close()
        results = self.post(path)
        if os.path.isfile(path):
            os.remove(path)
        return results
