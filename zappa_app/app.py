from flask import Flask, request, render_template
from app_robot import App_Robot

app = Flask(__name__)
app_robot = App_Robot()

@app.route('/')
def index():
    """
    Renders the html homepage.
    PARAMS: None
    RETURNS: 
        render_template: Rendering of homepage
    """    
    return render_template('index.html')

@app.route('/robot', methods=['GET','POST'])
def robot_route():
    """
    Renders the robot app UI
    PARAMS: None
    RETURNS: 
        render_template: Rendering of robot app UI
    """
    if request.method == "POST":
        try:
            file = request.files["file.txt"]     
            results = app_robot.post_file(file)
        except:
            cmd = request.form['command']
            results = app_robot.post_cmd(cmd)
        return render_template('robot/index_post.html',report=results)
    return render_template('robot/index_get.html')


# We only need this for local development.
if __name__ == '__main__':
    app.run()