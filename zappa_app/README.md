# Robot App on AWS Lambda using Zappa

## 1. Usage

1. ensure application is ready with requirements file
2. create virtual environment in app dir: `python -m venv venv` 
3. install requirements in activated environment:
    
    ```sh
    source ./venv/bin/activate
    pip install -r requirements.txt
    ```
    
    note that we included requests==2.28.2 because [urllib<2](https://stackoverflow.com/questions/76414514/cannot-import-name-default-ciphers-from-urllib3-util-ssl-on-aws-lambda-us)
4. configure aws user permissions
5. configure zappa settings. ensure that apigateway is enabled
6. run `zappa dev deploy`
7. use `zappa tail dev` to debug using logs

