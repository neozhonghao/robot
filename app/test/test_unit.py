import pytest
from src.robot import Robot

class Test_Robot():
    """
    Unit test for robot.py
    """
    
    def test_valid(self):
        """
        Unit test for valid function
        """
        robot = Robot()
        assert robot.valid({"x":2, "y":3, "f":"NORTH"}) == True
        assert robot.valid({"x":-1, "y":3, "f":"NORTH"}) == False

    def test_place(self):
        """
        Unit test for place function
        """
        robot = Robot()
        robot.place(4,0,"EAST")
        assert robot.state == {"x":4, "y":0, "f":"EAST"}
        robot = Robot()
        robot.place(3,10,"EAST")
        assert robot.state == None

    def test_move(self):
        """
        Unit test for move function
        """
        robot = Robot()
        robot.place(4,1,"WEST")
        robot.move()
        assert robot.state == ({"x":3, "y":1, "f":"WEST"})

        robot = Robot()
        robot.place(4,1,"EAST")
        robot.move()
        assert robot.state == ({"x":4, "y":1, "f":"EAST"})