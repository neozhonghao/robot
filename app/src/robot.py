class Robot():
    def __init__(self):
        self.state = None
        self.valid_xy = {"x":{"MIN":0,"MAX":4},
                         "y":{"MIN":0,"MAX":4}}
        self.f_vals = ["NORTH", "SOUTH", "EAST", "WEST"]
        self.mv_vals = {"NORTH":{"x":0, "y":1}, "SOUTH":{"x":0, "y":-1},
                        "EAST":{"x":1, "y":0}, "WEST":{"x":-1, "y":0}}
        self.right_vals = {"NORTH":"EAST", "EAST":"SOUTH", "SOUTH":"WEST", "WEST":"NORTH"}
        self.left_vals = {"NORTH":"WEST", "WEST":"SOUTH", "SOUTH":"EAST", "EAST":"NORTH"}
    
    def valid(self, state):
        if state:
            valid_x = self.valid_xy["x"]["MIN"] <= state["x"] <= self.valid_xy["x"]["MAX"]
            valid_y = self.valid_xy["y"]["MIN"] <= state["y"] <= self.valid_xy["y"]["MAX"]
            valid_f = state["f"] in self.f_vals
            return valid_x and valid_y and valid_f
        else:
            return False
        
    def place(self,x,y,f):
        state = {"x":x, "y":y, "f":f}
        if self.valid(state):
            self.state = state
            
    def move(self):
        if self.valid(self.state):
            x = self.state["x"]
            y = self.state["y"]
            f = self.state["f"]
            x_mv = self.mv_vals[f]["x"]
            y_mv = self.mv_vals[f]["y"]
            state = {"x":x+x_mv, "y":y+y_mv, "f":f}
            if self.valid(state):
                self.state = state
            
    def right(self):
        if self.valid(self.state):
            f = self.state["f"]
            self.state["f"] = self.right_vals[f]
            
    def left(self):
        if self.valid(self.state):
            f = self.state["f"]
            self.state["f"] = self.left_vals[f]
            
    def report(self):
        if self.valid(self.state):        
            x = self.state["x"]
            y = self.state["y"]
            f = self.state["f"]
            return x,y,f