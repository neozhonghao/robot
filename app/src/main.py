from robot import Robot
import os
from argparse import ArgumentParser
import re

def valid_command(cmd):
    if cmd in ["MOVE", "LEFT", "RIGHT", "REPORT"]:
        return True
    r = re.compile("^PLACE [0-9]+,[0-9]+,[A-Z]+$")
    if r.match(cmd):
        return True

def execute_command(cmd, robot):
    if cmd == "MOVE":
        robot.move()
    elif cmd == "LEFT":
        robot.left()
    elif cmd == "RIGHT":
        robot.right()
    elif cmd == "REPORT":
        return robot.report()
    else:
        lst = cmd[6:].split(",")
        robot.place(int(lst[0]),int(lst[1]),lst[2])

def run_file(path):
    robot = Robot()
    with open(path, 'r') as f: 
        for line in f:
            cmd = line.strip()
            if valid_command(cmd):
                ret = execute_command(cmd, robot)
                if ret:
                    return f"{ret[0]},{ret[1]},{ret[2]}"
    return ""

def run_cmd():
    robot = Robot()
    while True:
        cmd = input()
        if valid_command(cmd):
            ret = execute_command(cmd, robot)
            if ret:
                return f"{ret[0]},{ret[1]},{ret[2]}"

def main():
    parser = ArgumentParser()
    parser.add_argument('--file')
    args = parser.parse_args()

    try:
        out = run_file(args.file)
    except:
        out = run_cmd()
    print(out)

if __name__ == '__main__':
    main()