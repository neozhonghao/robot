from waitress import serve
from flask import Flask, request,  render_template, jsonify
from werkzeug.utils import secure_filename
from secrets import token_hex
from main import run_file
import os

app = Flask(__name__)

@app.route('/', methods=["POST"])
def index():
    """
    Flask app to run the robot script.
    PARAMS: None (The POST object is a string variable text)
    RETURNS:
        json: json of the output from the robot script
    """    
    file = request.files["file.txt"]
    token = token_hex(nbytes=16)
    file.save(token)

    out = run_file(token)
    resp = {"out":out}

    os.remove(token)

    return jsonify(resp)

if __name__ == "__main__":
    serve(app, host="0.0.0.0", port=8000)