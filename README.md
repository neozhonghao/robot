# Toy Robot :robot:
This is the code base for the toy robot webapp.
A working demo of the webapp can be found on neozhonghao's [website](https://neozhonghao.herokuapp.com/robot)

## Introduction
```mermaid
flowchart LR
    subgraph Developer
    id1(src)
    end
    subgraph End User
    id2(front)
    end
    subgraph Kubernetes
    id3(Deploy)
    end
    id1 --> |build & deploy|id3    
    id2 --> |POST|id3
```

This is a simple illustration of how the webapp microservice works. The developer builds and develops the robot app in `src` folder. The dev-ops team will deploy it on a kubernetes cluster. The end user can access the webapp through a front end UI.