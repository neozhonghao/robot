import aiohttp
import asyncio
import time
    
async def fetch(session, url, files):
    async with session.post(url=url,data=files) as response:
        return await response.text()

async def async_requests():
    tasks = []
    url = "http://host.docker.internal:8000"
    n = 10000
    path = "file.txt"
    f_in = open(path, "rb")
    files = {"file.txt":("file.txt", f_in)}

    async with aiohttp.ClientSession() as session:
        for i in range(n):
            tasks.append(fetch(session,url,files))
        resp_lst = await asyncio.gather(*tasks)
    f_in.close()

    for resp in resp_lst:
        print(resp)
        
def main():    
    t0 = time.time()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(async_requests())
    print(time.time()-t0)

main()